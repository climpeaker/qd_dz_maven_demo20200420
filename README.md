# qd_dz_maven_demo20200420

#### 介绍
maven工具的使用和介绍

#### 课堂笔记

1.  maven项目的创建
```aidl
    1.1 配置阿里镜像
        如果是第一次使用maven，需要先使用dos命令手动创建一个 .m2 目录（在操作系统的个人目录下）
        手动拷入阿里镜像的setting.xml配置文件；（从本项目的gitee下载）
    1.2 构建maven项目输入项目坐标
        GroupId：以包名的命名形式，表示项目的组织名；
        ArtifactId：是项目名；
        这两个属性必须制定；结合version属性可以唯一确定一个Maven项目；
    1.3 maven项目的运行
        使用tomcat发布运行即可；
    1.4 maven项目开发过程中常用的命令
        重建maven依赖关系可以使用：项目右键 --- maven --- reimport 命令；
        另外：
            右侧maven命令窗口：（开发过程中使用这个两个命令即可）
            点开 Lifecycle ：
                clean：清理maven所编译的内容，以及依赖关系；
                install：编译安装项目到本地；
        操作步骤：
            项目编码之后，可以直接install，如果成功，则直接启动服务器；
            如果install失败，则需要重新构建编译maven项目：先执行 clean，在执行install；
        要求，在运行tomcat之前，保证install是成功的；            
    1.5 maven项目使用pom.xml文件进行依赖、编译、发布等配置    
        依赖管理：
            在pom文件的dependencies节点下使用 dependencie标签进行依赖配置；
            具体的每个jar包的配置格式请查询maven仓库中心：https://mvnrepository.com/
    1.6 maven项目构建失败问题解决
        manven项目构建失败一般是因为下载maven插件包或者maven仓库的jar包引起的；
        解决问题方式：
            1、改变一下联网方式；（切换移动网络）；
            2、删除.m2下的对应的仓库文件，重新刷新引入maven依赖；
            3、拷贝同版本的maven仓库文件，覆盖本机的对应的目录；
            4、设置-DarchetypeCatalog=internal
```
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
